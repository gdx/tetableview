//
//  TETableViewDataSource.h
//
//  Created by GDX on 12/4/4.
//  Copyright (c) 2012年 28 interactive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TETableViewItem.h"
#import "TEModel.h"

@protocol TETableViewDataSourceDelegate;

@interface TETableViewDataSource : NSObject <UITableViewDataSource, TEModelDelegate>

@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) TEModel *model;
@property (unsafe_unretained, nonatomic) id <TETableViewDataSourceDelegate> delegate;

- (UITableViewCell *)cellWithTableView:(UITableView *)tableView item:(TETableViewItem *)item;
- (TETableViewItem *)itemForIndexPath:(NSIndexPath *)indexPath;

@end


@protocol TETableViewDataSourceDelegate <NSObject>

- (void)itemDidChangeWithDataSource:(TETableViewDataSource *)dataSource;

@end