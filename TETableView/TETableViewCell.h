//
//  TETableViewCell.h
//
//  Created by GDX on 12/4/4.
//  Copyright (c) 2012年 28 interactive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TETableViewCell : UITableViewCell

@property (strong, nonatomic) id object;

- (void)loadView;
+ (CGFloat)cellHeightWithObject:(id)object;

@end
